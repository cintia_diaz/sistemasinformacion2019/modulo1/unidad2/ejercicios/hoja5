﻿USE ciclistas;

/*
  CONSULTA 1.1:	Nombre y edad de los ciclistas que NO han ganado etapas.
*/
  -- c1: ciclistas que han ganado etapas
  SELECT
    DISTINCT e.dorsal 
  FROM etapa e;

  SELECT * FROM ciclista c LEFT JOIN (SELECT DISTINCT e.dorsal FROM etapa e) c1 ON c.dorsal=c1.dorsal;

  -- final
  SELECT c.nombre, c.edad 
    FROM ciclista c 
    LEFT JOIN 
    (
      SELECT
        DISTINCT e.dorsal 
      FROM etapa e
    ) c1
    ON c.dorsal=c1.dorsal
    WHERE c1.dorsal IS NULL;


 /*
  CONSULTA 1.2:	Nombre y edad de los ciclistas que NO han ganado puertos.
*/

  -- c1:  ciclistas que han ganado puertos
  SELECT 
    DISTINCT p.dorsal 
  FROM puerto p;

  -- final
  SELECT 
    c.nombre, c.edad 
  FROM ciclista c
  LEFT JOIN (SELECT DISTINCT p.dorsal FROM puerto p) c1 
  ON c.dorsal=c1.dorsal 
  WHERE c1.dorsal IS NULL;

  

  /*
    CONSULTA 1.3: Listar el director de los equipos que tengan ciclistas que NO hayan ganado NINGUNA etapa.
  */
   
    -- equipos que tienen ciclistas que no hayan ganado ninguna etapa

    SELECT 
      DISTINCT c.nomequipo
    FROM ciclista c 
    LEFT JOIN 
    (
      SELECT
        DISTINCT e.dorsal 
      FROM etapa e
    ) c1
    ON c.dorsal=c1.dorsal
    WHERE c1.dorsal IS NULL;

    -- director de los equipos 
      SELECT e.director 
      FROM equipo e 
      JOIN 
        (
          SELECT DISTINCT c.nomequipo
            FROM ciclista c 
          LEFT JOIN 
            (
              SELECT
                DISTINCT e.dorsal 
              FROM etapa e
            ) c1
          ON c.dorsal=c1.dorsal
          WHERE c1.dorsal IS NULL
        ) c2 ON e.nomequipo=c2.nomequipo;
      
  /*
    CONSULTA 1.4: Dorsal y nombre de los ciclistas que NO hayan llevado algún maillot.
  */

  -- ciclistas que hayan llevado algún maillot
  SELECT 
    DISTINCT l.dorsal 
  FROM lleva l;


  -- ciclistas que no han llevado algún maillot
  SELECT 
    c.dorsal, c.nombre 
  FROM ciclista c 
  LEFT JOIN 
    (
     SELECT DISTINCT l.dorsal FROM lleva l
    ) c1 
  ON c.dorsal=c1.dorsal 
  WHERE c1.dorsal IS NULL; 

  /*
    CONSULTA 1.5:	Dorsal y nombre de los ciclistas que NO hayan llevado el maillot amarillo NUNCA.
  */

    -- ciclistas que han llevado el maillot amarillo alguna vez
    SELECT 
      DISTINCT l.dorsal 
    FROM lleva l 
    JOIN maillot m 
    ON l.código = m.código
    WHERE m.color='Amarillo'; 
    

    -- final
    SELECT c.dorsal, c.nombre 
    FROM ciclista c 
    LEFT JOIN 
      (
        SELECT 
         DISTINCT l.dorsal 
        FROM lleva l 
        JOIN maillot m 
        ON l.código = m.código
        WHERE m.color='Amarillo'        
      ) c1
    ON c.dorsal=c1.dorsal
    WHERE c1.dorsal IS NULL;


 /*
   CONSULTA 1.6: Indicar el numetapa de las etapas que NO tengan puertos.
 */

      -- etapas que tienen puertos
      SELECT 
        DISTINCT p.numetapa 
      FROM puerto p;


      -- final
      SELECT e.numetapa
      FROM etapa e 
      LEFT JOIN 
        (
          SELECT DISTINCT p.numetapa FROM puerto p
        ) c1 
      ON e.numetapa=c1.numetapa
      WHERE c1.numetapa IS NULL;


 /*
   CONSULTA 1.7: Indicar la distancia media de las etapas que NO tengan puertos.
 */

    -- consulta 6: etapas que no tienen puertos, calculo la distancia media

    SELECT AVG(e.kms) distanciaMedia
      FROM etapa e 
      LEFT JOIN 
        (
          SELECT DISTINCT p.numetapa FROM puerto p
        ) c1 
      ON e.numetapa=c1.numetapa
      WHERE c1.numetapa IS NULL;



 /*
   CONSULTA 1.8: Listar el número de ciclistas que NO hayan ganado alguna etapa.
 */

     -- ciclistas que han ganado etapas
      SELECT DISTINCT e.dorsal FROM etapa e;
     -- ciclistas que NO han ganado etapas
      SELECT c.dorsal 
        FROM ciclista c 
        LEFT JOIN (SELECT DISTINCT e.dorsal FROM etapa e) c1 
        USING (dorsal) 
        WHERE c1.dorsal IS NULL;
     -- numero de ciclistas que no han ganado etapas
      SELECT COUNT(*) numero
      FROM ciclista c 
      LEFT JOIN (SELECT DISTINCT e.dorsal FROM etapa e) c1
      USING (dorsal) 
      WHERE c1.dorsal IS NULL;

 /*
   CONSULTA 1.9: Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tengan puerto.
 */

      -- etapas que tienen puertos
      SELECT DISTINCT p.numetapa FROM puerto p;
      -- etapas que no tienen puertos
      SELECT 
        e.numetapa
      FROM etapa e 
      LEFT JOIN (SELECT DISTINCT p.numetapa FROM puerto p) c1
      USING(numetapa)
      WHERE c1.numetapa IS NULL;

      -- dorsal de los ciclistas que han ganado alguna etapa que no tiene puertos

      SELECT DISTINCT e.dorsal 
      FROM etapa e 
      JOIN 
        (SELECT 
              e.numetapa
            FROM etapa e 
            LEFT JOIN (SELECT DISTINCT p.numetapa FROM puerto p) c1
            USING(numetapa)
            WHERE c1.numetapa IS NULL
        ) c1 
      USING (numetapa);
     

 /*
   CONSULTA 1.10: Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos.
 */

  /**
    SOLUCION ERRONEA

    -- dorsal en etapas sin puertos - dorsal en etapas con puertos
    
    -- dorsal en etapas sin puertos
    SELECT DISTINCT c1.dorsal FROM (
      SELECT e.dorsal, e.numetapa FROM etapa e LEFT JOIN (SELECT DISTINCT p.numetapa FROM puerto p) c1 ON e.numetapa=c1.numetapa
        WHERE c1.numetapa IS NULL
      ) c1;

    -- dorsal en etapas con puertos
    SELECT  DISTINCT p.dorsal FROM puerto p;


    -- final
    SELECT c1.dorsal FROM
      (
        SELECT DISTINCT c1.dorsal FROM (
        SELECT e.dorsal, e.numetapa FROM etapa e LEFT JOIN (SELECT DISTINCT p.numetapa FROM puerto p) c1 ON e.numetapa=c1.numetapa
          WHERE c1.numetapa IS NULL
        ) c1
      ) c1
      LEFT JOIN
      (
        SELECT  DISTINCT p.dorsal FROM puerto p
      ) c2
      ON c1.dorsal=c2.dorsal
      WHERE c2.dorsal IS NULL;
  **/

 /** SOLUCION **/

    -- ciclistas que han ganado etapas
      SELECT DISTINCT e.dorsal FROM etapa e;

    -- ciclistas que han ganado etapas con puertos 
      SELECT DISTINCT p.dorsal FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa;

    -- final
      SELECT c1.dorsal FROM 
        (SELECT DISTINCT e.dorsal FROM etapa e) c1 
        LEFT JOIN 
        (SELECT DISTINCT p.dorsal FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa) c2
        USING (dorsal)
        WHERE c2.dorsal IS NULL;

      